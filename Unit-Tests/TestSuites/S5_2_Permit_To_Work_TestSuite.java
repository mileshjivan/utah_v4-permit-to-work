/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;

import java.io.FileNotFoundException;
import org.junit.Test; 

/**
 *
 * @author SKhumalo
 */
public class S5_2_Permit_To_Work_TestSuite extends BaseClass {

    static TestMarshall instance;

    public S5_2_Permit_To_Work_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.coreBeta;

        //*******************************************
    }

    @Test
    public void S5_2_Permit_To_Work_TestSuite() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\S5_2_Permit_To_Work_QA01S5_2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR1-Capture Permit To Work - Main Scenario
    @Test
    public void FR1_Capture_Permit_To_Work_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Permit To Work v5.2\\FR1-Capture Permit To work - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_Capture_Permit_To_Work_AlternateScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Permit To Work v5.2\\FR1-Capture Permit To work - Alternate Scenario1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_Capture_Permit_To_Work_AlternateScenario2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Permit To Work v5.2\\FR1-Capture Permit To work - Alternate Scenario2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_Capture_Permit_To_Work_OptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Permit To Work v5.2\\FR1-Capture Permit To work - Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //UC PMW 0102 - Updating the Permit request – Permit Approval - Main Scenario
    @Test
    public void UC_PMW_0102_Updating_The_Permit_Request_Permit_Approval_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Permit To Work v5.2\\UC PMW 0102 - Updating the Permit request - Permit Approval - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_PMW_0102_Updating_The_Permit_Request_Permit_Approval_AlternateScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Permit To Work v5.2\\UC PMW 0102 - Updating the Permit request - Permit Approval - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_PMW_0102_Updating_The_Permit_Request_Permit_Approval_AlternateScenario2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Permit To Work v5.2\\UC PMW 0102 - Updating the Permit request - Permit Approval - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //UC PMW 0103 - Updating the Permit request – Permit Preparation Approval - Main Scenario
    @Test
    public void UC_PMW_0103_Updating_The_Permit_Request_Permit_Preparation_Approval_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Permit To Work v5.2\\UC PMW 0103 - Updating the Permit request - Permit Preparation Approval - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_PMW_0103_Updating_The_Permit_Request_Permit_Preparation_Approval_AlternateScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Permit To Work v5.2\\UC PMW 0103 - Updating the Permit request - Permit Preparation Approval - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_PMW_0103_Updating_The_Permit_Request_Permit_Preparation_Approval_AlternateScenario2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Permit To Work v5.2\\UC PMW 0103 - Updating the Permit request - Permit Preparation Approval - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_PMW_0103_Updating_The_Permit_Request_Permit_Preparation_Approval_AlternateScenario3() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Permit To Work v5.2\\UC PMW 0103 - Updating the Permit request - Permit Preparation Approval - Alternate Scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR3_Permit_To_Work_Automatically_Expires_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Permit To Work v5.2\\FR3-Permit To Work Automatically Expires - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR3_Permit_To_Work_Automatically_Expires_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Permit To Work v5.2\\FR3-Permit To Work Automatically Expires - Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR4_View_Linked_JSA_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Permit To Work v5.2\\FR4-View Linked JSA - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR2_Capture_Permit_Acceptance_Main_Scenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Permit To Work v5.2\\FR2_Capture_Permit_Acceptance_Main_Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR2_Capture_Permit_Acceptance_Alternate_Scenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Permit To Work v5.2\\FR2_Capture_Permit_Acceptance_Alternate_Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test

    public void UC_PMW_02_02_Approve_Reject_Execution_Main_Scenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Permit To Work v5.2\\UC_PMW_02_02_Approve_Reject_Execution_Main_Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_PMW_02_02_Approve_Reject_Execution_Alternate_Scenario_1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Permit To Work v5.2\\UC_PMW_02_02_Approve_Reject_Execution_Alternate_Scenario_1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_PMW_02_02_Approve_Reject_Execution_Alternate_Scenario_2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Permit To Work v5.2\\UC_PMW_02_02_Approve_Reject_Execution_Alternate_Scenario_2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_PMW_02_02_Approve_Reject_Execution_Alternate_Scenario_3() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Permit To Work v5.2\\UC_PMW_02_02_Approve_Reject_Execution_Alternate_Scenario_3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void UC_PMW_02_02_Approve_Reject_Execution_Alternate_Scenario_4() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Permit To Work v5.2\\UC_PMW_02_02_Approve_Reject_Execution_Alternate_Scenario_4.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}
