/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Win;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;

/**
 *
 * @author Ethiene
 */
@KeywordAnnotation(
        Keyword = "Testing",
        winAppTest = true
)

public class testingWin extends BaseClass {
    
    String error = "";
    
    public testingWin() {
        
    }
    
    public TestResult executeTest() {
        if (!add()) {
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Completed Capturing Suggestions and Innovation - Optional Scenario.");
    }
    
    public boolean add() {
        
        if (!WinDriverInstance.clickElementbyName("Four")) {
            error = "Failed to click on number four";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Sucessfully click Four");
        
        if (!WinDriverInstance.clickElementbyName("Plus")) {
            error = "Failed to click on number plus";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Sucessfully click Plus");
        
        if (!WinDriverInstance.clickElementbyName("Five")) {
            error = "Failed to click on number five";
            return false;
        }
        
         narrator.stepPassedWithScreenShot("Sucessfully click Five");
        
        if (!WinDriverInstance.clickElementbyName("Equals")) {
            error = "Failed to click on number Equals";
            return false;
        }
        
        return true;
    }
    
}
