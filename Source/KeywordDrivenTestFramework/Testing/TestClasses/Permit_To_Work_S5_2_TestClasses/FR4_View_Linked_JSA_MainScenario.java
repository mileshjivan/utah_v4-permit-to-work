/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Permit_To_Work_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Permit_To_Work_V5_2_PageObjects.Permit_To_Work_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR4-View Linked JSA - Main Scenario",
        createNewBrowserInstance = false
)

public class FR4_View_Linked_JSA_MainScenario extends BaseClass
{

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR4_View_Linked_JSA_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!View_Linked_JSA())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Viewed Linked JSA");
    }

    public boolean View_Linked_JSA()
    {

        //2.Permit planning
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.PermitPlanning_Tab()))
        {
            error = "Failed to wait for 2.Permit planning tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.PermitPlanning_Tab()))
        {
            error = "Failed to click the 2.Permit planning tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 2.Permit planning tab.");

        //Preparation approval
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.PreparationApproval_Dropdown()))
        {
            error = "Failed to wait for Preparation approval dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.PreparationApproval_Dropdown()))
        {
            error = "Failed to click the Preparation approval dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Preparation approval dropdown.");

        //Permit approval select
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.Project_Option(getData("Preparation approval"))))
        {
            error = "Failed to wait for Preparation approval option: " + getData("Preparation approval");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Project_Option(getData("Preparation approval"))))
        {
            error = "Failed to click the Preparation approval option: " + getData("Preparation approval");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Preparation approval option: " + getData("Preparation approval"));

        //2.5 Linked JSA
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.LinkedJSA_Panel()))
        {
            error = "Failed to wait for 2.5 Linked JSA panel.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.LinkedJSA_Panel()))
        {
            error = "Failed to click the 2.5 Linked JSA panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 2.5 Linked JSA panel.");

        //Click Linked JSA record
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.ClickLinkedJSARecord()))
        {
            error = "Failed to wait for Linked JSA record";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.ClickLinkedJSARecord()))
        {
            error = "Failed to click on Linked JSA record";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Linked JSA record.");
        pause(4000);
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.JSA_ProcessFlow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.JSA_ProcessFlow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        pause(4000);
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        return true;
    }

}
